# Action Recognition on Motus Domum dataset

## I3D

[config file](https://gitlab.com/sat-metalab/forks/mmaction2/-/blob/feature/test_motus_domum/configs/recognition/i3d/i3d_r50_2e_motus_rgb_finetuned.py)

[Motus Domum dataset information](https://gitlab.com/sat-metalab/forks/mmaction2/-/tree/feature/test_motus_domum/data/motus_domum)

Pretraining dataset [Kinetics](https://deepmind.com/research/open-source/kinetics)

Model/metric | Training_loss | Test_Acc (top-1) | Test_Acc (mean_class_Acc) |
--- | --- | ---| ---|
I3D_from_scratch | 0.7728 | 92.31 % | 93.06 % |
I3D_Kinetics | 0.0891 | 97.44 % | 91.67 % |

### confusion matrix

![Alt text](./work_dirs/motus_domum_i3d/confusion_matrix.png)

## Slowonly

[config file](https://gitlab.com/sat-metalab/forks/mmaction2/-/blob/feature/test_motus_domum/configs/recognition/slowonly/slowonly_imagenet_50e_motus_domum_finetuned.py)

[Motus Domum dataset information](https://gitlab.com/sat-metalab/forks/mmaction2/-/tree/feature/test_motus_domum/data/motus_domum)

Pretraining dataset [Kinetics](https://deepmind.com/research/open-source/kinetics)

Model/metric | Training_loss | Test_Acc (top-1) | Test_Acc (mean_class_Acc) |
--- | --- | ---| ---|
Slowonly_Kinetics | 0.4358 | 46.15% (top-5: 97.44%)| 41.67% |


### confusion matrix

![Alt text](./work_dirs/motus_domum_slowonly_finetuned4/confusion_matrix.png)

## Slowfast

[config file](https://gitlab.com/sat-metalab/forks/mmaction2/-/blob/feature/test_motus_domum/configs/recognition/slowfast/slowfast_imagenet_50e_motus_domum_finetuned.py)

[Motus Domum dataset information](https://gitlab.com/sat-metalab/forks/mmaction2/-/tree/feature/test_motus_domum/data/motus_domum)

Pretraining dataset [Kinetics](https://deepmind.com/research/open-source/kinetics)

Model/metric | Training_loss | Test_Acc (top-1) | Test_Acc (mean_class_Acc) |
--- | --- | ---| ---|
Slowonly_Kinetics |  0.6887 | 92.31% (top-5: 100%)| 93.75% |


### confusion matrix

![Alt text](./work_dirs/motus_domum_slowfast_finetuned/matrix.png)