#Abstract

Based on the [Diataxis](https://diataxis.fr/) guide, this documentation is a tutorial on how to train
a deep learning (DL) model for the task of activity recognition from videos (aka. video recognition) using
[mmaction2 repository](https://github.com/open-mmlab/mmaction2). Even though the [mmaction2 repository](https://github.com/open-mmlab/mmaction2) provides the users with
how-to-guides on its various functionalities, the information is often scattered. This calls
for a more integrated and comprehensive tutorial, where new users can find information for training a model
all in one place. We gather the information from [mmaction2 tutorials](https://github.com/open-mmlab/mmaction2/tree/master/docs/tutorials), 
[answers to the issues](https://github.com/open-mmlab/mmaction2/issues), 
and the lessons we learned over time by using this repository.

Throughout this tutorial, we learn about data preparation (both for videos and skeleton data)
, we set up configuration files for training and testing DL models, we go over fine-tunning
DL models, and finally we learn how to test and deploy a model for real-world usage.

It is important to note, that teaching the basics of DL is out of the scope of this tutorial.
We include references for interested readers to learn more about DL. Therefore, this tutorial 
assumes theoretical knowledge of DL and elaborates on the practical steps of training a DL model and deploying it
in [Livepose](https://gitlab.com/sat-metalab/livepose) for action recognition.

# Introduction

At Metalab we strive to bridge HARV and immersive art and create technological arts or artistic technologies! An important part of
understanding a scene is recognizing the actions performed by humans and their interactions with the environment. From a technical
point of view, this falls under the Human Action Recognition from Videos (HARV)(including live feed). 

Human Action Recognition from Videos (HARV) is one of the popular applications of computer vision. In recent
HARV received significant attention from research and engineering communities for its many usages such as
medical care, security, self-driving, and entertainment. The emergence of Deep Learning (DL) techniques
marked a leap forward in the performance of HARV models and quickly turned into the state-of-the-art technique
for HARV. We propose to use the state-of-the-art advances in this field, therefore, this tutorial walks you through 
how to train and deploy a deep learning model for HARV.

## mmaction2 repository

[Mmaction2](https://github.com/open-mmlab/mmaction2) provides an open-source implementation of state-of-the-art models and tools for HARV applications. 
The repository provides support and persistently adds recent models, published at top computer vision conferences.
The fact that mmaction2 is an open-source repository is aligned with what we thrive on at Metalab. Therefore, we integrate
mmaction2 into our tools and provide thorough documentation on how to train a model using mmaction2.

The repository already provides a great deal of [documentation and tutorials](https://github.com/open-mmlab/mmaction2/tree/master/docs); however, we find it time-consuming and
challenging for new users to find their way around the repository. Therefore, we are proving a complimentary step-by-step tutorial on
how to train a DL model using mmaction2 repository.

To start, refer to the [mmaction2 introduction page](https://github.com/open-mmlab/mmaction2) to browse through available tools in the repo.
In the next step, follow the [mmaction2 installation instruction](https://github.com/open-mmlab/mmaction2/blob/master/docs/install.md) to
install the repo on your device. In order to verify your installation, refer to 
[mmaction2 installation verification](https://mmaction2.readthedocs.io/en/latest/install.html#verification).

## DL training definition

Each DL model consists of a collection of parameters to determine the importance and relativity of input and intermediate
features. In order for a model to have a correct mapping from input to the output (prediction), these parameters have to be tuned. The DL
model learns to tune these parameters by examining and finding the patterns in the data. This iterative training process is achieved through 
risk minimization.

## what is pretraining and fine-tuning?

DL models are data hungry, meaning that for a given task, the more data you have the better prediction you gain. Although, there have 
been many advances in other methods to reduce the dependency on the abundance of data, but to this day, learning from abundant data points
(supervised learning) is one of the most popular types of DL training.

Often the case with real-world problems is a lack of labeled training data, given how expensive is to produce annotated data. Luckily, it has 
been shown that training on a large-scale related dataset before training on one's specific dataset is helpful for most of the compute vision
tasks. This process is known as pre-training. But what does it exactly mean? 

We can initialize the parameters of a DL model using different methods (e.g., random, zero, etc). These parameters start with their
initialized values and change during training. Suppose, we take a large-scale dataset and train and DL model on that dataset
. Suppose in the step we take the trained model's parameters values and use them as initialization and continue training on our dataset. 
In this scenario, the process of training our model on a large-scale related dataset is called pre-training and continuing the training
on our dataset is called fine-tuning.

# Training steps

## Dataset preparation for Video/Images:

The first step to training is preparing data. mmaction2 repo supports several publicly available video datasets, you can find them in
[supported dataset page](https://mmaction2.readthedocs.io/en/latest/supported_datasets.html). They also provide thorough documentation
for how to prepare these datasets on [supported dataset preparation page.](https://github.com/open-mmlab/mmaction2/blob/master/docs/data_preparation.md)

mmaction2 repository also supports the use of custom video datasets for training DL models. The information is found in the  
[custom dataset page.](https://github.com/open-mmlab/mmaction2/blob/master/docs/tutorials/3_new_dataset.md).

### Quick dataset preparation

We provide some scripts for quick preparation of the dataset. These steps assume you already have mmaction2 installed and the 
virtual environment activated on your computer. Additionally, you need to install ffmpeg and scikit learn for data processing 
`sudo apt install ffmpeg` and `pip install sklearn`.

<details open>
<summary>Step 1: data folder structure</summary>
<br>
In the first step, prepare the video dataset under the `data` folder. The folder structure should be as below. Action classes (labels) are marked as 00 to NN. 
Place the input samples of each class/label xx under the class_xx folder.

```
data
└── dataset_name/
    ├── videos/
    │   ├── class_00/
    │   │   ├── video_name1.mp4
    │   │   ├── video_name2.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   ├── class_01/
    │   │   ├── video_name1.mp4
    │   │   ├── video_name2.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   └── class_02/
    │       └── ...
    └──

```
</details>


<details open>
<summary>Step 2: video to RGB frames</summary>
Having the structure above, run the `python video_to_rawframes.py dataset_name`. The `dataset_name` is the same as the top directory name in step 1.

This script will generate the RGB frames under `datadataset_name/rawframes` folder with the below structure. 
The folder and image structure below, is sufficient for verification of this step.

```
data
└── dataset_name/
    ├── videos/
    │   ├── class_00/
    │   │   ├── video_name1.mp4
    │   │   ├── video_name2.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   ├── class_01/
    │   │   ├── video_name1.mp4
    │   │   ├── video_name2.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   └── class_02/
    │       └── ...
    ├── rawframes/
    │   ├── class_00/
    │   │   └── video_name1/
    │   │       ├── img_00001.jpg
    │   │       ├── img_00002.jpg 
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg           
    │   │   └── video_name2/
    │   │       ├── img_00001.jpg
    │   │       ├── img_00002.jpg 
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg 
    │   │   └── ...
    │   │   └── video_nameN/
    │   │       ├── img_00001.jpg
    │   │       ├── img_00002.jpg 
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg 
    │   ├── class_01/
    │   │   └── video_name1/
    │   │       ├── img_00001.jpg
    │   │       ├── img_00002.jpg 
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg           
    │   │   └── video_name2/
    │   │       ├── img_00001.jpg
    │   │       ├── img_00002.jpg 
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg 
    │   │   └── ...
    │   │   └── video_nameN/
    │   │       ├── img_00001.jpg
    │   │       ├── img_00002.jpg 
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg 
    │   └── ...
    └──

```

</details>

<details open>
<summary>Step 3: generating annotation files </summary>
This step generates the annotation files for train, validation and test splits of the dataset. Run `python datasplit_generation.py dataset_name` to
output three .txt files containing rows with each row presenting rawframes_directory, number of RGB frames, and class label for each video.

The sets are created sklearn stratify with the 70, 15, 15 percentage for train, validation, and test sets respectively.

**verification** of this step is for the annotation txt file looking like below, with the columns
representing the clip_directory, the number of frames, and the class label respectively.

```
class_00/cam_1_clip_2_30f 290 0
class_09/cam_2_clip_3_f30 321 9
class_05/cam_1_clip_2_f30 475 5
class_00/cam_2_clip_0_30f 400 0
class_10/cam_2_clip_2_f30 680 10
```

</details>

## Dataset preparation for skeleton models (target: PoseC3D)

Pose estimation is a component of PoseC3D model. It applies a top-down technique by first running a person detector to 
identify subjects in the scene and then running an individual pose estimator on each of the detected bounding boxes. Therefore
the first step is install `mmdetection` and `mmpose`. 

Once these two source codes are installed, you can use the [pose extraction script](./tools/data/skeleton/ntu_pose_extraction.py). After replacing 
the installation root of mmdetection and mmpose; `mmdet_root` and `mmpose_root` place holders, respectively; you can follow the below steps:

<details open>
<summary>Step 1: data folder structure </summary>
you can prepare videos in a directory with following format:


```
data
└── videos/
    ├── data_split/
    │   ├── class_00/
    │   │   ├── video_name1.mp4
    │   │   ├── video_name2.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   ├── class_01/
    │   │   ├── video_name1.mp4
    │   │   ├── video_name2.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   └── class_02/
    │       └── ...
    └──

```

The `data_split` refers to `train, test, valid` split. Additionally, it is important for the class folders to be named as above (
e.g., class_00, class_01). Otherwise, you have to modify the script to run for other labeling formats. This step of processing takes a while to complete.

</details>

<details open>
<summary>Step 2: creating pickle file </summary>
Once the is sorted as above, you can run the script using `python ntu_pose_extraction.py path_to_data_split_folder output.pkl`.
The script will save the pose data in the output.pkl file (in the same directory as the script). For the format of the saved skeleton in the pkl file please refer to [PoseC3D annotations format.](./tools/data/skeleton/README.md)

**verification**: In order to assure the correctness of this step, read the pkl file into a numpy container and compare
the data with the [PoseC3D annotations format.](./tools/data/skeleton/README.md)
</details>

## Pick a Model

Over the years, researchers broke down action recognition into several categories, depending
on what specification is expected from the output. For example, temporal action localization
is the application where the outputs are the class label as well as the start time and end time
of the action. 

mmaction2 repository supports several applications listed in [mmaction2 intro page.](https://github.com/open-mmlab/mmaction2)
In this tutorial, we focus on action recognition with RGB or skeleton data as input.

## Configuration files

mmaction2 handles the model input variables using configuration files. You can learn more about the config files  
in their tutorial on [learn about config.](https://github.com/open-mmlab/mmaction2/blob/master/docs/tutorials/1_config.md)

The config files are under `configs/recognition/model_name`. For each model (e.g., i3d) you modify one of the
existing config templates to your use case. There is a template.py under `configs/recognition/i3d/`. The important parameters to set to start training are discussed in this section.

<details open>
<summary>Dataset parameters </summary>
The dataset parameters should be set as below:

`dataset_type = 'RawframeDataset'`

`data_root = 'data/dataset_name/rawframes/'`

`data_root_val = 'data/dataset_name/rawframes/'`

`ann_file_train = 'data/dataset_name/annotation_train.txt'`

`ann_file_val = 'data/dataset_name/annotation_valid.txt'`

`ann_file_test = 'data/dataset_name/annotation_test.txt'`

additionally, to inference on the test data (rather than validation data) make sure to set the ann_file=ann_file_test under `data["test"]` setting.
</details>

<details open>
<summary>Other parameters </summary>
The `num_classes` under `model` parameters should be set to a number of action classes/labels in your dataset.

You will need to specify a working directory by setting `work_dir` parameters. This is where the output files and logs are stored.

Finally, adjust the `videos_per_gpu` under  `data` based on your GPU capacity. Failing to adjust this parameter,
will result in a memory error.

The `total_epochs` parameter should be set for both training from scratch and fine-tuning to the desired number of iterations over the whole dataset.

**Important Note:** For the inference to work, the test_cfg should be moved from top level under `model` parameters. (see [PR#629](https://github.com/open-mmlab/mmaction2/pull/629))

</details>

### Distributed Computation and Linear Scaling Rule

This repository supports distributed computations. For it to work correctly, it is important to change the default learning rate in the config files linked to the [result log](METALAB_RESULTS.md) if 
you are using more than one GPU. 

The LR can be adjusted based on the linear scaling rule; meaning the LR is proportional to #GPUs * #Videos_per_GPU. For example if lr=0.1 for 1 GPU x 2 videos per GPU, it should be set to lr=0.6 for 3 GPUs x 4 videos per GPU. 
Failing to adjust the LR results in unsuccessful training.

## fine tuning

mmaction2 provides many pre-trained models on large-scale popular datasets. 
Additionally, the [fine-tuning doc](https://github.com/open-mmlab/mmaction2/blob/master/docs/tutorials/2_finetune.md) provides a guide on how to fine-tune the pre-trained models on a 
custom dataset. Briefly, To use pre-trained models, download the checkpoints from the [model zoo](https://mmaction2.readthedocs.io/en/latest/recognition_models.html) and store them. 
The `load_from` parameter in the config file should be set to the checkpoint directory. 
use `resume_from` only when the training has been interrupted

## Training

There is documentation about how to train a model on the [mmaction2 training page.](https://github.com/open-mmlab/mmaction2/blob/master/docs/getting_started.md#train-a-model)
Briefly, to start training run `python tools/train.py path_to_config_file --validate --deterministic`.
In order to use pre-trained models, download the checkpoints from the [model zoo](https://mmaction2.readthedocs.io/en/latest/recognition_models.html) and store them. 
The `load_from` parameter in the config file should be set to the checkpoint directory.
use `resume_from` only when the training has been interrupted.
For more training option refer to the readme under `configs/recognition/model_name` (ex for [i3d readme](https://github.com/open-mmlab/mmaction2/blob/master/configs/recognition/i3d/README.md) )

**Note:** for distributed training use `tools/dist_train.sh`

### How do I know if my model is training?

A model is training if the training starts and the loss parameter is decreasing in the training log over the epochs. 
This process is slow but eventually, after a few epochs, you should see a decrease in loss value. The image below 
demonstrates what to expect when training starts. ![image](./training.png) 

#### What hyperparameters to tune?

Often the default parameters set by mmaction2 config files are sufficient for successful training. 
In cases where it is not true, the immediate hyperparameters to tune are learning rate and an increasing number of epochs.
You can also refer to this [Online resource for training tips.](http://karpathy.github.io/2019/04/25/recipe/)

## Testing

For inference run `python tools/test.py path_to_config_file_under_workdirectory path_to_saved_checkpoints_under_work_directory --eval mean_class_accuracy top_k_accuracy --out
path_to_json.out`. In order to use the follow-up scripts, save the inference results with the `output.json` name under the working directory.

## Confusion Matrix

The confusion matrix allows for error analysis of supervised machine learning algorithms. It sheds light on what the
algorithm predicts for given examples in the test portion of the data. 

To draw the confusion matrix for a model run `python tools/analysis/confusion_matrix.py path_to_output.json path_to_annotation_test.txt path_to_save_matrix_image title_for_confusion_matrix`
This script uses the output.json file generated by the `test.py script`. 

## Deployment on Livepose

Your model is ready for deployment once it reaches good performance on the test set or your live feed. In this step,
you can deploy your model to livepose for action recognition. In order to use your trained model, set the 
`load_from` parameter in the config file to the directory of your model saved checkpoint (found in your
working directory). Then follow the frame_liveaction.py and pose_liveaction.py templates to feed appropriate action detection parameters.


#resources (links)

Here we introduce some free resources for readers starting on DL:
* [Deep Learning Book](https://www.deeplearningbook.org/)
* [Deep Learning AI youtube by Andrew Ng](https://www.youtube.com/c/Deeplearningai)
* [Deep lizard courses](https://deeplizard.com/courses) 
  * This series also include practical tips