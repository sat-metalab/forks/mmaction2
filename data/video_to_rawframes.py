import os, sys, argparse
from tqdm import tqdm
import rosbag
import cv2
import numpy as np

FPS = 30

global args

def get_directory():
    # get dataset name
    dataset_name = args.dataset
    rawframes_directory = os.path.join("./", dataset_name, "rawframes")
    video_directory = os.path.join("./", dataset_name, "videos")
    return rawframes_directory, video_directory, dataset_name


def make_directory(directory_name):
    if not os.path.exists(directory_name):
        os.mkdir(directory_name)
    else:
        pass

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", type=str, help="dataset name")
    parser.add_argument('--rosbag-stream', default="Color", choices=["Color","Depth","Infrared"], type=str, help="rosbag stream")
    args = parser.parse_args()

    rawframes_directory, video_directory, dataset_name = get_directory()
    make_directory(rawframes_directory)

    for classes in tqdm(os.listdir(video_directory), desc="creating class folders"):
        print(classes)
        if not classes.startswith("class_"):
            continue
        if os.path.isdir(os.path.join(video_directory, classes)):
            make_directory(os.path.join(rawframes_directory, classes))
            for videos in tqdm(
                os.listdir(os.path.join(video_directory, classes)),
                desc="converting videos to frames",
            ):
                video_name, extension = os.path.splitext(videos)
                make_directory(os.path.join(rawframes_directory, classes, video_name))
                video_path = os.path.join(video_directory, classes, videos)
                image_path = os.path.join(rawframes_directory, classes, video_name)
                if extension.endswith("bag"):
                    # https://github.com/mlaiacker/rosbag2video/blob/master/rosbag2video.py
                    counter = 1
                    for topic, msg, t in rosbag.Bag(video_path).read_messages():                    
                        if topic.endswith('image/data') and topic.find(args.rosbag_stream) != -1:
                            pix_fmt=None
                            if msg.encoding.find('YUYV')!=-1:
                                pix_fmt=cv2.COLOR_YUV2BGR_YUYV
                            else:
                                print(f"unsupported format {msg.encoding}")
                                sys.exit(1)
                            np_arr = np.frombuffer(msg.data, np.uint8)
                            mat = np.reshape(np_arr, (msg.height,msg.width,2))
                            cv_image = cv2.cvtColor(mat, pix_fmt)
                            image_filename = '%s/img_%05d.jpg' % (image_path,counter)
                            cv2.imwrite(image_filename,cv_image)
                            counter += 1
                else:
                    command = "ffmpeg -i {0} -vf fps={1} {2}/img_%05d.jpg -hide_banner -loglevel error".format(
                        video_path, FPS, image_path
                    )
                    print(command)
                    os.system(command)
        else:
            print(
                "Video directory does not match the template. Check the readme for instructions"
            )
            sys.exit()
