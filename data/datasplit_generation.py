import os, glob, re, argparse
from tqdm import tqdm
from sklearn import model_selection


def write_annotation_file(split, x, y, dataset_name):
    annotation_file_path = os.path.join(
        "./", dataset_name, ("annotation_" + str(split) + ".txt")
    )
    f = open(annotation_file_path, "w")
    for data_point in zip(x, y):
        f.write(
            " ".join(str(s) for s in data_point[0]) + " " + str(data_point[1]) + "\n"
        )
    f.close()


def get_directory():
    # get dataset name
    parser = argparse.ArgumentParser()

    parser.add_argument("dataset", type=str, help="dataset name")
    parser.add_argument(
        "--sampled", action="store_true", help="add if data is sampled", default=False
    )
    args = parser.parse_args()
    dataset_name = args.dataset
    sampled = args.sampled
    if sampled:
        rawframes_directory = os.path.join("./", dataset_name, "rawframes_sampled")
    else:
        rawframes_directory = os.path.join("./", dataset_name, "rawframes")
    return rawframes_directory, dataset_name


def split_data(rawframes_directory):
    # getting all the videos name and their lables
    # x is the input data and y the label
    x = []
    y = []
    for classes in tqdm(os.listdir(rawframes_directory), desc="classes"):
        labeles = int(re.findall(r"\d+", classes)[0])

        for videos in tqdm(
            os.listdir(os.path.join(rawframes_directory, classes)), desc="videos"
        ):

            video_path = os.path.join(classes, videos)
            number_of_frames = len(
                glob.glob1(os.path.join(rawframes_directory, video_path), "*.jpg")
            )
            x.append((video_path, number_of_frames))
            y.append(labeles)

    # stratify data to train, valid, test
    x_train_temp, x_test, y_train_temp, y_test = model_selection.train_test_split(
        x, y, test_size=0.10, random_state=42
    )
    x_train, x_valid, y_train, y_valid = model_selection.train_test_split(
        x_train_temp, y_train_temp, test_size=0.18, random_state=42
    )

    return x_train, y_train, x_valid, y_valid, x_test, y_test


if __name__ == "__main__":

    rawframes_directory, dataset_name = get_directory()
    x_train, y_train, x_valid, y_valid, x_test, y_test = split_data(rawframes_directory)
    write_annotation_file("train", x_train, y_train, dataset_name)
    write_annotation_file("valid", x_valid, y_valid, dataset_name)
    write_annotation_file("test", x_test, y_test, dataset_name)
