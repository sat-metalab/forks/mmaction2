import json
import argparse
from sklearn import metrics
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def parse_arugments():

    parser = argparse.ArgumentParser()
    parser.add_argument("path_output_json", type=str, help="path_output_json")
    parser.add_argument("path_test_annotation", type=str, help="path_test_annotation")
    parser.add_argument("path_to_save", type=str, help="path_to_save")
    parser.add_argument("plot_title", type=str, help="plot_title")
    args = parser.parse_args()
    output_json_path = args.path_output_json
    annotation_file_path = args.path_test_annotation
    plot_title = args.plot_title
    path_to_save = args.path_to_save

    return output_json_path, annotation_file_path, path_to_save, plot_title


if __name__ == "__main__":

    output_json_path, annotation_file_path, path_to_save, plot_title = parse_arugments()
    output_json_file = open(output_json_path)
    output_json = np.array(json.load(output_json_file))
    y_pred = np.argmax(output_json, axis=1)

    test_label = pd.read_csv(annotation_file_path, sep=" ", header=None)
    y_true = test_label[2].to_numpy()

    conf_matrix = metrics.confusion_matrix(y_true, y_pred)
    plt.figure(figsize=(6, 4))
    sns.heatmap(conf_matrix, annot=True, cmap="Blues")

    plt.title(plot_title)
    plt.savefig(path_to_save)
